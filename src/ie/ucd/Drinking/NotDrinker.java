package ie.ucd.Drinking;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.*;

public class NotDrinker extends Person {

	public NotDrinker() {

	}

	@Override
	public boolean drink(Drink arg0) {
		if (arg0 instanceof AlcoholicDrink) {
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public boolean eat(Food arg0) {
		// TODO Auto-generated method stub
		return true;
	}

}
