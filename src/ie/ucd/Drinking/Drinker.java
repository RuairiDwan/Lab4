package ie.ucd.Drinking;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.*;

public class Drinker extends Person {
	private int numberOfDrinks;

	public Drinker() {

	}

	@Override
	public boolean drink(Drink arg0) {
		// TODO Auto-generated method stub
		if (arg0 instanceof AlcoholicDrink) {
			numberOfDrinks++;
		} 
		return true;
	}

	@Override
	public boolean eat(Food arg0) {
		// TODO Auto-generated method stub
		return true;

	}

	public boolean isDrunk() {

		if (this.numberOfDrinks > this.getWeight() / 10) {
			System.out.println("This person is drunk");
			return true;
		} else {
			System.out.println("This person is not drunk");
			
			return false;
		}
	}
}
