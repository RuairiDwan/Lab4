package ie.ucd.Drinking;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.items.Wine;
import ie.ucd.items.WineType;
import ie.ucd.people.*;

public class Main {

	public static void main(String[] args) {
		
		Drink Coke = new Drink("Coke", 12.0) {
		};
		AlcoholicDrink Whiskey = new AlcoholicDrink("Whiskey", 10.0, 5.0) {
		};
		
		Drinker Stephen = new Drinker();
		Stephen.setWeight(80.0);
		
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		Stephen.drink(Whiskey);
		
		Stephen.isDrunk();
		
	}

}
